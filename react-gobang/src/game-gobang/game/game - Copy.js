import React, { Component } from "react";
import { Row } from "react-bootstrap";
import GobangBoard from "../board/board";
import { BottonBar, PlayerBar } from "../bars/bars";
import { createMatrix, copyMatrix, checkWin } from "../matrix/matrix";
import "./game.css";

export default class GobangGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      matrix: createMatrix(15, 15, 0),
      active: createMatrix(15, 15, 0),
      isBlack: true,
      firstPlayer: 0,
      modeIsMulti: true,
      history: [
        {
          matrix: createMatrix(15, 15, 0)
        }
      ],
      currStep: 0,
      player1: "Player 1",
      player2: "Player 2",
      nameEntered: true,
      seconds: 60
    };
    this.gameOver = false;
    this.winner = "";

    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);

    this._startGame = this._startGame.bind(this);
    this._resetNames = this._resetNames.bind(this);
    this._setPlayer1Name = this._setPlayer1Name.bind(this);
    this._setPlayer2Name = this._setPlayer2Name.bind(this);
    this._updateMatrix = this._updateMatrix.bind(this);
    this._switchPlayer = this._switchPlayer.bind(this);
    this._changeMode = this._changeMode.bind(this);
    this._newGame = this._newGame.bind(this);
  }

  startTimer() {
    this.setState({ seconds: 60 });
    if (this.timer === 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    if (seconds === -1) return;
    this.setState({
      seconds: seconds
    });
  }

  _setPlayer1Name(event) {
    this.setState({ player1: event.target.value });
  }

  _setPlayer2Name(event) {
    this.setState({ player2: event.target.value });
  }

  _startGame() {
    this.setState({ nameEntered: true });
    this.startTimer();
  }

  _updateMatrix(x, y) {
    const matrix_ = copyMatrix(15, 15, this.state.matrix);
    const index = this.state.currStep + 1;
    const history_ = this.state.history.slice(0, index);
    const isBlack_ = this.state.isBlack;
    const active_ = createMatrix(15, 15, 0);
    active_[x][y] = "🔴";
    if (matrix_[x][y]) return;
    matrix_[x][y] = isBlack_ ? "⚫️" : "⚪️";

    this.setState(
      {
        matrix: matrix_,
        isBlack: !isBlack_,
        active: active_,
        history: history_.concat([
          {
            matrix: matrix_
          }
        ]),
        currStep: index
      },
      () => {
        if (!this.state.modeIsMulti && !this.state.isBlack)
          this._computerSelectBox();
      }
    );
    this.startTimer();
  }

  /*
   AI Version
   Random for now
  */
  _computerSelectBox() {
    const matrix_ = copyMatrix(15, 15, this.state.matrix);
    let xRand;
    let yRand;
    let cell = 1;

    while (!this.gameOver && cell > 0) {
      xRand = Math.floor(Math.random() * 15);
      yRand = Math.floor(Math.random() * 15);
      cell = matrix_[xRand][yRand];
    }

    if (!this.gameOver) this._updateMatrix(xRand, yRand);
  }

  _switchPlayer() {
    const new1stPlayer = this.state.firstPlayer ? 0 : 1;
    this.setState({
      firstPlayer: new1stPlayer
    });
    this.startTimer();
  }

  _resetNames() {
    this.setState({
      nameEntered: false
    });
    this.startTimer();
  }

  _changeMode() {
    this.setState(
      {
        modeIsMulti: !this.state.modeIsMulti
      },
      () => {
        var event = {
          target: {
            value: !this.state.modeIsMulti ? "Computer" : "Player 2"
          }
        };
        this._setPlayer2Name(event);
        this._newGame();
      }
    );
  }

  _newGame() {
    this.setState({
      matrix: createMatrix(15, 15, 0),
      active: createMatrix(15, 15, 0),
      isBlack: true,
      history: [
        {
          matrix: createMatrix(15, 15, 0)
        }
      ],
      currStep: 0
    });
    this.gameOver = false;
    this.winner = "";
    this.startTimer();
  }

  render() {
    // check if there is a winner
    const result = checkWin(this.state.matrix);
    if (result.win) {
      this.gameOver = true;
      const firstPlayer = this.state.firstPlayer
        ? this.state.player2
        : this.state.player1;
      const secondPlayer = this.state.firstPlayer
        ? this.state.player1
        : this.state.player2;
      this.winner = this.state.currStep % 2 === 1 ? firstPlayer : secondPlayer;
    }

    return (
      <Row className="gameContainer mx-0">
        <PlayerBar
          switchPlayer={this._switchPlayer}
          firstPlayer={this.state.firstPlayer}
          isBlack={this.state.isBlack}
          player1={this.state.player1}
          player2={this.state.player2}
          nameEntered={this.state.nameEntered}
          setPlayer1Name={this._setPlayer1Name}
          setPlayer2Name={this._setPlayer2Name}
          submitNames={this._startGame}
          resetNames={this._resetNames}
        />
        <GobangBoard
          matrix={this.state.matrix}
          active={this.state.active}
          modeIsMulti={this.state.modeIsMulti}
          isBlack={this.state.isBlack}
          updateMatrix={this._updateMatrix}
          gameOver={this.gameOver}
          winner={this.winner}
          nameEntered={this.state.nameEntered}
        />
        <BottonBar
          nameEntered={this.state.nameEntered}
          changeMode={this._changeMode}
          newGame={this._newGame}
          seconds={this.state.seconds}
          resetTimer={this.startTimer}
        />
      </Row>
    );
  }
}

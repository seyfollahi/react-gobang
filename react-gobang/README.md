# Gobang 
A simple chess game forked from https://github.com/ruihuasui/react-gobang .<br/>


## Build
1. setup the react packages
```
$ npm install
```
2. run on the local host: http://localhost:3000/
```
$ npm start
```

## Test
1. setup the react packages
```
$ npm test
```

## Player v.s. AI version
Random for now
